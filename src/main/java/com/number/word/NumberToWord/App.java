package com.number.word.NumberToWord;

import java.text.DecimalFormat;

public class App {

  private static final String[] tens = {
    "",
    " ten",
    " twenty",
    " thirty",
    " forty",
    " fifty",
    " sixty",
    " seventy",
    " eighty",
    " ninety"
  };

  private static final String[] nums = {
    "",
    " one",
    " two",
    " three",
    " four",
    " five",
    " six",
    " seven",
    " eight",
    " nine",
    " ten",
    " eleven",
    " twelve",
    " thirteen",
    " fourteen",
    " fifteen",
    " sixteen",
    " seventeen",
    " eighteen",
    " nineteen"
  };

  App() {}

  private static String convertLessThanOneThousand(int number) {
    String tillNow;

    if (number % 100 < 20){
      tillNow = nums[number % 100];
      number /= 100;
    }
    else {
      tillNow = nums[number % 10];
      number /= 10;

      tillNow = tens[number % 10] + tillNow;
      number /= 10;
    }
    if (number == 0) return tillNow;
    return nums[number] + " hundred" + tillNow;
  }


  public String convert(long number) {
    if (number == 0) { return "zero"; }

    String snumber = Long.toString(number);

    String mask = "000000000000";
    DecimalFormat df = new DecimalFormat(mask);
    snumber = df.format(number);

    int billions = Integer.parseInt(snumber.substring(0,3));
    int millions  = Integer.parseInt(snumber.substring(3,6));
    int hundredThousands = Integer.parseInt(snumber.substring(6,9));
    int thousands = Integer.parseInt(snumber.substring(9,12));

    String tradBillions;
    switch (billions) {
    case 0:
      tradBillions = "";
      break;
    case 1 :
      tradBillions = convertLessThanOneThousand(billions)
      + " billion ";
      break;
    default :
      tradBillions = convertLessThanOneThousand(billions)
      + " billion ";
    }
    String result =  tradBillions;

    String tradMillions;
    switch (millions) {
    case 0:
      tradMillions = "";
      break;
    case 1 :
      tradMillions = convertLessThanOneThousand(millions)
         + " million ";
      break;
    default :
      tradMillions = convertLessThanOneThousand(millions)
         + " million ";
    }
    result =  result + tradMillions;

    String tradHundredThousands;
    switch (hundredThousands) {
    case 0:
      tradHundredThousands = "";
      break;
    case 1 :
      tradHundredThousands = "one thousand ";
      break;
    default :
      tradHundredThousands = convertLessThanOneThousand(hundredThousands)
         + " thousand ";
    }
    result =  result + tradHundredThousands;

    String tradThousand;
    tradThousand = convertLessThanOneThousand(thousands);
    result =  result + tradThousand;

    // remove extra spaces!
    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
  }

}
