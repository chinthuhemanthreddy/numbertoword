package com.number.word.NumberToWord;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	private App app;
	/* @BeforeClass
	    public void initApp() {
	        app = new App();
	    }*/
	 
	 @Test
	    public void testSum() {
		 app = new App();
	        assertEquals("fifty billion one hundred", app.convert(50000000100L));
	        assertEquals("one", app.convert(1));
	        assertEquals("ninety two", app.convert(92));
	        assertEquals("three thousand three hundred thirty three", app.convert(3333));
	        assertEquals("two hundred seventeen", app.convert(217));
	        assertEquals("one billion four hundred seventy four million eight hundred thirty six thousand four hundred seventy two", app.convert(1474836472));
	        
	        
	    }
}
